import axios from 'axios'
import IFuncionalidade from "../negocio/permissoes/IFuncionalidade"
import IMenu from "../negocio/permissoes/IMenu"

export default class ServicoDePermissoes {
    constructor() {
    }

    public async consulte(): Promise<IMenu> {
        return new Promise<IMenu>((resolve, reject) => {
            let menu: IMenu

            axios.get<IFuncionalidade[]>(`http://localhost:3001/permissoes`).then(res => {
                menu = {
                    funcionalidades: res.data
                }

                resolve(menu)
            })
        })
    }
}