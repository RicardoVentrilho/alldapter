import IFuncionalidade from './IFuncionalidade'

export default interface IMenu {
    funcionalidades: IFuncionalidade[]
}