import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MenuSuperior from './MenuSuperior';

describe('<MenuSuperior />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<MenuSuperior />);
    const menuSuperior = getByTestId('MenuSuperior');

    expect(menuSuperior).toBeInTheDocument();
  });
});