import React, { Component } from 'react'
import { Navbar, Nav, Form, FormControl, Button, Container } from 'react-bootstrap'

import IMenu from './../../negocio/permissoes/IMenu'

import './MenuSuperior.scss'
import ServicoDePermissoes from '../../servicos/ServicoDePermissoes'

class MenuSuperior extends Component<{}, IMenu> {
  servicoDePermissoes: ServicoDePermissoes

  constructor(props: IMenu) {
    super(props)
    this.servicoDePermissoes = new ServicoDePermissoes()

    this.state = {
      funcionalidades: []
    }
  }

  public async componentWillMount() {
    this.servicoDePermissoes.consulte().then(menu =>
      this.setState(menu)
    )
  }

  render() {
    return  (
      <Navbar fixed="top" bg="info" variant="dark" className="navbar-light">
        <Nav className="mr-auto">
        { this.state.funcionalidades.map(x => (
          <Nav.Item key={x.codigo} >
            <Nav.Link href="#">{x.nome}</Nav.Link>
          </Nav.Item>
        )) }
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-light">Search</Button>
        </Form>
      </Navbar>
    )
  }
}

export default MenuSuperior