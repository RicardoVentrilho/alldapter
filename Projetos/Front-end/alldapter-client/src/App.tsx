import React from 'react';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import MenuSuperior from './componentes/MenuSuperior/MenuSuperior';

const App: React.FC = () => {
  return (
    <Container>
      <MenuSuperior />
      <Jumbotron>
        <h1 className="header">
          Welcome To React-Bootstrap TypeScript Example
        </h1>
      </Jumbotron>
    </Container>
  );
};

export default App;